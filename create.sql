DROP TABLE IF EXISTS WORKING_TIME;
DROP TABLE IF EXISTS SHOP;

/**
 * SHOP table only contains the name of stores
 */
CREATE TABLE SHOP (
  name VARCHAR(50) NOT NULL PRIMARY KEY
);

/**
 * A dedicated table storing open time of stores
 */
CREATE TABLE WORKING_TIME (
  start TIME,
  end TIME,
  day CHAR(1),
  shop VARCHAR(50),

  CONSTRAINT WTPK
    PRIMARY KEY(start, end, day, shop),
    
  CONSTRAINT WTFK
    FOREIGN KEY(shop) REFERENCES SHOP(name)
      ON DELETE CASCADE ON UPDATE CASCADE
);


/**
 * Examples of data in tables
 */

/*
  INSERT INTO SHOP (name) VALUES ('a');
  INSERT INTO SHOP (name) VALUES ('b');
  INSERT INTO SHOP (name) VALUES ('c');

  INSERT INTO WORKING_TIME (start, end, day, shop)
  VALUES ("8:00", "18:00", 1, 'a');

  INSERT INTO WORKING_TIME (start, end, day, shop)
  VALUES ("20:00", "23:00", 1, 'a');

  INSERT INTO WORKING_TIME (start, end, day, shop)
  VALUES ("10:00", "12:00", 4, 'b');

  INSERT INTO WORKING_TIME (start, end, day, shop)
  VALUES ("14:00", "18:00", 4, 'b');
*/