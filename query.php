<?php
  /**
   *  PHP version 5.6
   */


  /**
   * sample class recording a store's open time
   * close time and corresponding day of week
   */
  class duration {
    private $start, $end, $day, $open;
    
    public function __construct($d, $s='0', $e='0') {
      $this->start = $s;
      $this->end = $s;
      $this->day = $d;

      if ($s == '0')
        $this->open = false;
      else
        $this->open = true;
    }
    
    public function is_open() {
      return $this->open;
    }

    public function get_day() {
      return $this->day;
    }

    public function get_start() {
      return $this->start;
    }

    public function get_end() {
      return $this->end;
    }
  }

  /**
   * Utility function connecting DB
   */
  function db_conn() {
    $servername = "XXXX";
    $username = "XXXX";
    $password = "XXXX";
    $dbname = "XXXX";

    $conn = new mysqli($servername, $username, $password, $dbname);
    
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    return $conn;
  }


  /**
   * Interface for problem 1-1
   */
  function check_open($store) {
    $conn = db_conn();

    $week_day = date('w');    //get current day of week [1,2,3,4,5,6,7]
    $curr_time = date('g:i'); //get current time  e.g. 20:35

    $query = "SELECT * FROM WORKING_TIME WHERE start <= ? AND end >= ? AND day = ? AND shop = ?";

    //prepare statement for DB security
    $stmt = $conn->prepare($query);
    $stmt->bind_param('ssss', $curr_time, $curr_time, $week_day, $store);

    $stmt->execute();
    $result = $stmt->get_result();

    $stmt->close();
    $conn->close();

    return $result->num_rows > 0;
  }


  /**
   * Interface for problem 1-2
   */
  function find_open_day($store) {
    $conn = db_conn();
    $query = "SELECT start, end, day FROM WORKING_TIME WHERE shop = ? ORDER BY day, start";

    //prepare statement for DB security
    $stmt = $conn->prepare($query);
    $stmt->bind_param('s', $store);

    $stmt->execute();
    $result = $stmt->get_result();

    $stmt->close();
    $conn->close();

    $current = 1;       //counter for recording current day of week
    $retval = array();  //return value of an array of store's open duration

    //iterate through querying results
    while ($row = $result->fetch_assoc()) {
      
      $day_of_query = $row['day']-'0';
      $start = $row['start'];
      $end = $row['end'];

      //append missed day of week to the array, as these days are closed
      while ($current < $day_of_query)
        array_push($retval, new duration($current++));

      //append queried open time
      array_push($retval, new duration($day_of_query, $start, $end));

      //always reset day counter one day before queried day
      $current = $day_of_query+1;
    }

    //append missed day of week
    while ($current <= 7)
      array_push($retval, new duration($current++));

    return $retval;
  }

  
  /**
   * Example of status checking
   */
  $res = check_open('a');
  if ($res)
    echo "open";
  else
    echo "close";

  /**
   * Example of listing open time
   */
  $open_day = find_open_day('a');

  //iterate from Monday to Sunday
  foreach ($open_day as $idx => $day) {
    $open = $day->is_open();
    if (!$open)
      //print <day of week> + <close> 
      echo $day->get_day(), " close", "<br/>" ;
    else
      //print <day of week> <open time> <close time>
      echo $day->get_day(), "  ", $day->get_start(), "->", $day->get_end(), "<br/>";
  }
?>